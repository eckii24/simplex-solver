import copy
from fractions import Fraction

""" 
---------------------------------------------------------------------
---------------------------------------------------------------------
SIMPLEX-SOLVER   by Matthias Eck
---------------------------------------------------------------------
---------------------------------------------------------------------
Dieses Skript nimmt ein lineares Optimierungsproblem in Form einer
Matrix entgegen und kann für diese Matrix die Lösung ermitteln.
Die Matrix ist als verschachtelte Liste zu übergeben. Beispiel:
MATRIX = [
  [0, "x", "y", "z", "RS"],
  ["G", 1, 2, 3, 4],
  ["a", 5, 6, 7, 8],
  ["b", 9, 0, 1, 2],
]

Hinweise zur Matrix:
- Die erste Zeile und die erste Spalte enthält die Bezeichnungen
  der Matrix. Alle anderen Felder müssen nummerisch sein.
- Eine freie Variable kann in der Bezeichnung mit '+' gekennzeichnet
  werden. Diese muss anfangs in der ersten Zeile stehen.
- Eine gesperrte Variable kann durch eine '0' in der Bezeichnung
  gekennzeichnet werden. Diese muss anfangs in der ersten Spalte stehen
---------------------------------------------------------------------
---------------------------------------------------------------------
"""

# MATRIX = [
#    [0, "x1", "x2", "x3", "x4+", "x5+", "RS"],
#    ["G", 8, 8, 8, 1, 1, 0],
#    ["a", 0,-4,-6,-1,0,26],
#    ["b", -5,-3,-2,-1,0,32],
#    ["c", -2,-5,-4,0,-1,30],
#    ["d", -4,-3,-4,0,-1,34]
# ]

MATRIX = [
    [0, 'x11', 'x12', 'x21', 'x22', 'rs'],
    ['-k', 8, 5, 6, 4, 0],
    ['0', 1, 1, 0, 0, 4],
    ['0', 0, 0, 1, 1, 2],
    ['0', 1, 0, 1, 0, 3],
    ['0', 0, 1, 0, 1, 3]
]


def validate_matrix(matrix):
    if type(matrix) != list:
        return False

    if type(matrix[0]) != list:
        return False

    inner_size = len(matrix[0])

    for item in matrix:
        if len(item) != inner_size:
            return False

    for i in range(1, len(matrix)):
        for j in range(1, len(matrix[0])):
            if type(matrix[i][j]) != int and type(matrix[i][j]) != float:
                return False

    return True


def calc_pivot_row(matrix, pivot, result=None):
    if not result:
        result = copy.deepcopy(matrix)
    for i in range(1, len(matrix[0])):
        if i != pivot[1]:
            result[pivot[0]][i] = matrix[pivot[0]][i] / matrix[pivot[0]][pivot[1]]
    return result


def calc_pivot_column(matrix, pivot, result=None):
    if not result:
        result = copy.deepcopy(matrix)
    for i in range(1, len(matrix)):
        if i != pivot[0]:
            result[i][pivot[1]] = - matrix[i][pivot[1]] / matrix[pivot[0]][pivot[1]]
    return result


def calc_pivot(matrix, pivot, result=None):
    if not result:
        result = copy.deepcopy(matrix)
    result[pivot[0]][pivot[1]] = 1 / matrix[pivot[0]][pivot[1]]
    return result


def calc_other_fields(matrix, pivot, result=None):
    if not result:
        result = copy.deepcopy(matrix)

    for i in range(1, len(matrix)):
        for j in range(1, len(matrix[0])):
            if "0" in result[0][j] or result[0][j] == 0:
                result[i][j] = 0
            elif i != pivot[0] and j != pivot[1]:
                result[i][j] = matrix[i][j] - ((matrix[pivot[0]][j] * matrix[i][pivot[1]]) / matrix[pivot[0]][pivot[1]])
    return result


def swap_pivot_fields(matrix, pivot, result=None):
    if not result:
        result = copy.deepcopy(matrix)
    result[0][pivot[1]], result[pivot[0]][0] = matrix[pivot[0]][0], matrix[0][pivot[1]]
    return result


def print_matrix(matrix):
    for i in range(len(matrix)):
        printing = ""
        for j in range(len(matrix[0])):
            printing += str(number_to_fraction(matrix[i][j])) + " " * (15 - len(str(number_to_fraction(matrix[i][j]))))
            if j == 0 or j == len(matrix[0]) - 2:
                printing += "|    \t"
        print(printing)
        if i == 0 or i == 1:
            print("-" * (15 * (len(matrix[0]) + 1)))


def number_to_fraction(number):
    if type(number) == float:
        frac = Fraction(number).limit_denominator()
        return frac
    return number


def calc_current_table(matrix, pivot):
    result = copy.deepcopy(matrix)
    result[0][0] = matrix[0][0] + 1

    result = swap_pivot_fields(matrix, pivot, result)
    result = calc_pivot(matrix, pivot, result)
    result = calc_pivot_row(matrix, pivot, result)
    result = calc_pivot_column(matrix, pivot, result)
    result = calc_other_fields(matrix, pivot, result)

    return result


def calc_simplex_phase(matrix):
    for j in range(1, len(matrix[0]) - 1):
        if "+" in matrix[0][j]:
            return "0+"

    result = 0
    for i in range(2, len(matrix)):
        if "0" in matrix[i][0] or matrix[i][0] == 0:
            result += 10000
        elif not "+" in matrix[i][0] and matrix[i][-1] < 0:
            result += 100
        else:
            result += 1

    if result // 10000:
        return "0"
    elif result // 100:
        return "1"
    else:
        for j in range(1, len(matrix[0]) - 1):
            if matrix[1][j] < 0 and "0" not in matrix[0][j] and matrix[0][j] != 0:
                return "2"
        return "3"


def calc_pivot_element(matrix, phase):
    if phase == "0+":
        return calc_pivot_phase_0_plus(matrix)
    elif phase == "0":
        return calc_pivot_phase_0(matrix)
    elif phase == "1":
        return calc_pivot_phase_1(matrix)
    elif phase == "2":
        return calc_pivot_phase_2(matrix)
    else:
        print("Fehler in Phasenberechnung. Programm beendet.")
        print_matrix(matrix)
        exit(1)


def calc_pivot_phase_0_plus(matrix):
    for j in range(1, len(matrix[0]) - 1):
        if "+" in matrix[0][j]:
            for i in range(2, len(matrix)):
                if ("0" in matrix[i][0] or matrix[i][0] == 0) and matrix[i][j] != 0:
                    return (i, j)
            for i in range(2, len(matrix)):
                if "+" not in matrix[i][0] and matrix[i][j] != 0 and abs(matrix[i][j]) == 1:
                    return (i, j)
            for i in range(2, len(matrix)):
                if "+" not in matrix[i][0] and matrix[i][j] != 0:
                    return (i, j)


def calc_pivot_phase_0(matrix):
    for i in range(2, len(matrix)):
        if "0" in matrix[i][0] or matrix[i][0] == 0:
            for j in range(1, len(matrix[0]) - 1):
                if "0" not in matrix[0][j] and matrix[0][j] != 0 and matrix[i][j] != 0 and abs(matrix[i][j]) == 1:
                    return (i, j)
            for j in range(1, len(matrix[0]) - 1):
                if "0" not in matrix[0][j] and matrix[0][j] != 0 and matrix[i][j] != 0:
                    return (i, j)


def calc_pivot_phase_1(matrix):
    for i in range(2, len(matrix)):
        if matrix[i][-1] < 0 and "+" not in matrix[i][0]:
            for j in range(1, len(matrix[0]) - 1):
                if matrix[i][j] < 0 and "0" not in matrix[0][j] and matrix[0][j] != 0 and abs(matrix[i][j]) == 1:
                    return (i, j)
            for j in range(1, len(matrix[0]) - 1):
                if matrix[i][j] < 0 and "0" not in matrix[0][j] and matrix[0][j] != 0:
                    return (i, j)


def calc_pivot_phase_2(matrix):
    result = [[0, 0] for column in matrix[0]]

    for j in range(1, len(matrix[0]) - 1):
        if "0" not in matrix[0][j] and matrix[0][j] != 0 and matrix[1][j] < 0:
            result[j][1] = 99999999999
            for i in range(2, len(matrix)):
                if "+" not in matrix[i][0] and matrix[i][j] > 0 and matrix[i][-1] / matrix[i][j] < result[j][1]:
                    result[j][0] = i
                    result[j][1] = matrix[i][-1] / matrix[i][j]
            if result[j][1] == 99999999999:
                print("Matrix hat wegen Spalte {} unendlich viele Lösungen".format(j))
                exit(0)
            result[j][1] *= abs(matrix[1][j])

    pivot = None
    maxi = -1
    for j in range(1, len(result) - 1):
        if result[j][0] != 0 and result[j][1] > maxi:
            maxi = result[j][1]
            pivot = (result[j][0], j)

    return pivot


def check_if_solvable(matrix):
    for i in range(2, len(matrix)):
        if matrix[i][-1] < 0 and "+" not in matrix[i][0]:
            unsolvable = True
            for j in range(1, len(matrix[0]) - 1):
                if matrix[i][j] < 0:
                    unsolvable = False
                    break
            if unsolvable:
                print("Aufgabe wegen Zeile {} nicht lösbar.".format(i))
                print_matrix(matrix)
                exit(0)


def deleting_unrelevant_row(matrix):
    for i in range(2, len(matrix)):
        if ("0" in matrix[i][0] or matrix[i][0] == 0) and matrix[i][-1] == 0:
            deletable = True
            for j in range(1, len(matrix) - 1):
                if "0" not in matrix[0][j] and matrix[0][j] != 0 and matrix[i][j] != 0:
                    deletable = False
                    break
            if deletable:
                matrix.pop(i)
    return matrix


# ---------------------------------------------------------------------
# BERECHNUNG SIMPLEX TABLEAU
# ---------------------------------------------------------------------

matrix = copy.deepcopy(MATRIX)
if not validate_matrix(matrix):
    print("Fehlerhafte Eingabe der Matrix")
    exit(1)

while True:
    if matrix[0][0] > 100:
        print("Endlosschleife...")
        break

    check_if_solvable(matrix)
    matrix = deleting_unrelevant_row(matrix)
    print_matrix(matrix)

    phase = calc_simplex_phase(matrix)
    if phase == "3":
        print("Problem gelöst.")
        break

    pivot = calc_pivot_element(matrix, phase)
    print("Phase: {} mit Pivot-Element bei {} mit Wert {}".format(
        phase, pivot, number_to_fraction(matrix[pivot[0]][pivot[1]])))
    print()
    print()
    matrix = calc_current_table(matrix, pivot)
